package kittenlord.demo003;

import static org.assertj.core.api.Assertions.assertThat;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class ConfigurationTest {

	static private Configuration config;

	@BeforeAll
	static void setup() throws IOException {
		var content = String.join("\n", //
				"# comment", //
				"", //
				"# another comment", //
				"title=Test", //
				"window.width=800", //
				"window.height=600", //
				"# yet another comment", //
				"level.1.name=Level One", //
				"level.1.data=one.txt", //
				"level.2.name=Level Two", //
				"level.2.data=two.txt", //
				"level.3.name=Level Three", //
				"level.3.data=three.txt");
		config = new Configuration(new ByteArrayInputStream(content.getBytes()));
	}

	@Test
	void testGetString() {
		assertThat(config.getString("title")).isEqualTo("Test");
		assertThat(config.getString("level.%d.name", 1)).isEqualTo("Level One");
		assertThat(config.getString("level.%d.name", 2)).isEqualTo("Level Two");
		assertThat(config.getString("level.%d.name", 3)).isEqualTo("Level Three");
	}

	@Test
	void testGetInteger() {
		assertThat(config.getInteger("window.width")).isEqualTo(800);
		assertThat(config.getInteger("window.height")).isEqualTo(600);
	}

	@Test
	void testGetKeys() {
		assertThat(config.getKeys("level")).containsExactly("1", "2", "3");
	}
	
}
