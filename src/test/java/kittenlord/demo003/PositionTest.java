package kittenlord.demo003;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

class PositionTest {

	@Test
	void testMoved() {
		assertThat(new Position(5, 5).moved(Direction.UP, 3)).isEqualTo(new Position(5, 2));
		assertThat(new Position(5, 5).moved(Direction.RIGHT, 3)).isEqualTo(new Position(8, 5));
		assertThat(new Position(5, 5).moved(Direction.DOWN, 3)).isEqualTo(new Position(5, 8));
		assertThat(new Position(5, 5).moved(Direction.LEFT, 3)).isEqualTo(new Position(2, 5));
	}

	@Test
	void testDirectionTo() {
		assertThat(new Position(5, 5).directionTo(new Position(5, 2))).contains(Direction.UP);
		assertThat(new Position(5, 5).directionTo(new Position(8, 5))).contains(Direction.RIGHT);
		assertThat(new Position(5, 5).directionTo(new Position(5, 8))).contains(Direction.DOWN);
		assertThat(new Position(5, 5).directionTo(new Position(2, 5))).contains(Direction.LEFT);
		
		assertThat(new Position(5, 5).directionTo(new Position(5, 5))).isEmpty();
		assertThat(new Position(5, 5).directionTo(new Position(8, 8))).isEmpty();
	}
	
}
