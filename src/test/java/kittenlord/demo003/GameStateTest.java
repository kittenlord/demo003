package kittenlord.demo003;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;

import kittenlord.demo003.GameState.FieldType;

class GameStateTest {

	@Test
	void testGetters() {
		var state = state("xx.o");
		assertThat(state.getWidth()).isEqualTo(4);
		assertThat(state.getHeight()).isEqualTo(1);
		assertThat(state.getFieldAt(pos(0, 0))).isEqualTo(FieldType.PIECE);
		assertThat(state.getFieldAt(pos(1, 0))).isEqualTo(FieldType.PIECE);
		assertThat(state.getFieldAt(pos(2, 0))).isNull();
		assertThat(state.getFieldAt(pos(3, 0))).isEqualTo(FieldType.EMPTY);
	}

	@Test
	void testStringList() {
		assertThat(state("oxo", "xo.").toStringList()).containsExactly("oxo", "xo.");
		assertThat(state( //
				"......", //
				"..xox.", //
				"..xx..", //
				"......").toStringList()).containsExactly( //
						"xox", //
						"xx.");
	}

	@Test
	void testPiecesRemaining() {
		assertThat(state("xxx", "oxo").piecesRemaining()).isEqualTo(4);
	}

	@Test
	void testCanMove() {
		var state = state(".xxx.", "xxxxx", "xxoxx", "xxxxx", ".xxx.");
		assertThat(state.canMoveAnywhere()).isTrue();
		assertThat(allMoves(state)).containsExactly("(2,0)->(2,2)", "(0,2)->(2,2)", "(4,2)->(2,2)", "(2,4)->(2,2)");
		state.clickAt(pos(2, 0));
		assertThat(state.toOneLineString()).isEqualTo(".xxx.:xxxxx:xxoxx:xxxxx:.xxx.");
		state.clickAt(pos(2, 2));
		assertThat(state.toOneLineString()).isEqualTo(".xox.:xxoxx:xxxxx:xxxxx:.xxx.");
		assertThat(allMoves(state)).containsExactly("(0,1)->(2,1)", "(4,1)->(2,1)", "(2,3)->(2,1)");
		state.clickAt(pos(0, 1));
		state.clickAt(pos(2, 1));
		assertThat(state.toOneLineString()).isEqualTo(".xox.:ooxxx:xxxxx:xxxxx:.xxx.");
		assertThat(allMoves(state)).containsExactly("(3,1)->(1,1)", "(2,2)->(2,0)", "(0,3)->(0,1)", "(1,3)->(1,1)");

		assertThat(state("oxo", "xxx", "oxo").canMoveAnywhere()).isFalse();
	}

	// a shortcut for repetitive code
	private GameState state(String... lines) {
		return GameState.fromStringList(Arrays.asList(lines));
	}

	// a shortcut for repetitive code
	private Position pos(int x, int y) {
		return new Position(x, y);
	}

	/**
	 * Returns a list of all possible moves, represented as human-readable strings, ordered by row, then by column,
	 * first by start position, then by end position.
	 */
	private List<String> allMoves(GameState state) {
		var moves = new ArrayList<String>();
		for (int startY = 0; startY < 5; startY++) {
			for (int startX = 0; startX < 5; startX++) {
				var startPosition = new Position(startX, startY);
				for (int endY = 0; endY < 5; endY++) {
					for (int endX = 0; endX < 5; endX++) {
						var endPosition = new Position(endX, endY);
						if (state.canMove(startPosition, endPosition)) {
							moves.add(startPosition.toString() + "->" + endPosition.toString());
						}
					}
				}
			}
		}
		return moves;
	}

}
