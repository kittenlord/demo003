package kittenlord.demo003;

import java.awt.BorderLayout;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dialog.ModalityType;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Properties;

import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.table.DefaultTableModel;

/**
 * A Solitaire game.
 */
public class Game implements Runnable {

	private Configuration config = null;
	private GameState board = null;
	private String currentLevel = null;

	private JFrame frame;
	private Canvas canvas;

	/**
	 * To prevent occassional graphical distortions when painting canvas, we will first paint everything into this
	 * image, and only when everything is prepared, we will paint the image onto the canvas.
	 */
	private BufferedImage canvasBuffer;

	private BufferedImage background;
	private BufferedImage empty;
	private BufferedImage piece;
	private BufferedImage pieceSelected;

	private int windowWidth;
	private int windowHeight;
	private int boardTop;
	private int boardLeft;

	private Properties hiscore;
	private boolean hiscoreChanged = false;

	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Game());
	}

	@Override
	public void run() {
		try {
			config = Resources.getConfiguration();
			loadImages(config);
			hiscore = Resources.loadHiScore(config);
			canvas = createCanvas();
			canvas.setPreferredSize(new Dimension( //
					windowWidth = config.getInteger("window.width"),
					windowHeight = config.getInteger("window.height")));
			canvasBuffer = new BufferedImage(windowWidth, windowHeight, BufferedImage.TYPE_INT_ARGB);
			frame = new JFrame(config.getString("window.title"));
			frame.setIconImage(Resources.getImage(config.getString("image.application")));
			frame.setJMenuBar(createMenuBar());
			frame.add(canvas);
			frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
			frame.setVisible(true);
			frame.pack();
			frame.addWindowListener(new WindowAdapter() {

				@Override
				public void windowClosed(WindowEvent e) {
					if (hiscoreChanged) {
						Resources.saveHiScore(hiscore, config);
					}
				}

			});
			loadLevel(config.getString("default.level"));
		} catch (IOException ex) {
			JOptionPane.showMessageDialog(null, "Error loading configuration file", "Error", JOptionPane.ERROR_MESSAGE);
			return;
		}
	}

	private JMenuBar createMenuBar() {
		var bar = new JMenuBar();
		var app = new JMenu("App");
		app.add(item("About", () -> showAboutDialog()));
		app.add(item("Help", () -> showHelpDialog()));
		app.addSeparator();
		app.add(item("Quit", () -> frame.dispose()));
		bar.add(app);
		var levelMenu = new JMenu("Level");
		for (var level : config.getKeys("level")) {
			var name = config.getString("level.%s.name", level);
			levelMenu.add(item(name, () -> loadLevel(level)));
		}
		levelMenu.addSeparator();
		levelMenu.add(item("Show HiScore", () -> showHiScoreDialog()));
		bar.add(levelMenu);
		return bar;
	}

	private JMenuItem item(String title, Runnable action) {
		var item = new JMenuItem(title);
		item.addActionListener(e -> action.run());
		return item;
	}

	private Canvas createCanvas() {
		@SuppressWarnings("serial")
		var canvas = new Canvas() {

			@Override
			public void paint(Graphics g) {
				paintToBuffer(canvasBuffer.getGraphics());
				g.drawImage(canvasBuffer, 0, 0, null);
			}

			public void paintToBuffer(Graphics g) {
				if (null != background) {
					g.drawImage(background, 0, 0, null);
				} else {
					g.setColor(Color.GRAY);
					g.fillRect(0, 0, getWidth(), getHeight());
				}
				if (null == board) {
					return;
				}
				for (int y = 0; y < board.getHeight(); y++) {
					for (int x = 0; x < board.getWidth(); x++) {
						var pos = new Position(x, y);
						var i = boardLeft + piece.getWidth() * x;
						var j = boardTop + piece.getHeight() * y;
						if (board.contains(pos)) {
							if (board.hasPieceAt(pos)) {
								if (board.isSelected(pos)) {
									g.drawImage(pieceSelected, i, j, null);
								} else {
									g.drawImage(piece, i, j, null);
								}
							} else {
								g.drawImage(empty, i, j, null);
							}
						}
					}
				}
			}

			@Override
			public void update(Graphics g) {
				paint(g);
			}

		};
		canvas.addMouseListener(new MouseAdapter() {

			@Override
			public void mousePressed(MouseEvent e) {
				e.consume();
				if (null == board) {
					return;
				}
				int i = e.getX() - boardLeft;
				var j = e.getY() - boardTop;
				// we must check the left and top border like this, rather than (x < 0) || (y < 0)
				// because in Java the division operator rounds towards zero (instead of downwards)
				// therefore both (1 / 50) and (-1 / 50) are equal to 0
				if ((i < 0) || (j < 0)) {
					return;
				}
				var x = i / piece.getWidth();
				var y = j / piece.getHeight();
				if ((x < board.getWidth()) || (y < board.getHeight())) {
					boardClicked(x, y);
				}
			}

			private void boardClicked(int x, int y) {
				if (board.clickAt(new Position(x, y))) {
					if (!board.canMoveAnywhere()) {
						if (isNewHiScore()) {
							int hi = board.piecesRemaining();
							SwingUtilities.invokeLater(
									() -> JOptionPane.showMessageDialog(frame, "Game Over, New Hi Score: " + hi));
						} else {
							SwingUtilities.invokeLater(() -> JOptionPane.showMessageDialog(frame, "Game Over"));
						}
					}
				}
				canvas.repaint();
			}

		});
		return canvas;
	}

	private boolean isNewHiScore() {
		int previous = hiscore.containsKey(currentLevel) ? Integer.parseInt(hiscore.getProperty(currentLevel))
				: Integer.MAX_VALUE;
		int current = board.piecesRemaining();
		if (current < previous) {
			hiscore.setProperty(currentLevel, Integer.toString(current));
			hiscoreChanged = true;
			return true;
		}
		return false;
	}

	private void loadImages(Configuration config) {
		background = Resources.getImage(config.getString("image.background"));
		empty = Resources.getImage(config.getString("image.empty"));
		piece = Resources.getImage(config.getString("image.piece"));
		pieceSelected = Resources.getImage(config.getString("image.piece.selected"));
	}

	void loadLevel(String level) {
		try {
			board = GameState.fromStringList(Resources.getText(config.getString("level.%s.data", level)));
			currentLevel = board.toOneLineString();
			boardLeft = (windowWidth - board.getWidth() * piece.getWidth()) / 2;
			boardTop = (windowHeight - board.getHeight() * piece.getHeight()) / 2;
		} catch (IOException e) {
			JOptionPane.showMessageDialog(null, "Error loading level: " + level, "Error", JOptionPane.ERROR_MESSAGE);
			board = null;
			currentLevel = null;
		}
		canvas.repaint();
	}

	void showAboutDialog() {
		JOptionPane.showMessageDialog(frame,
				"Solitaire (demo003)\n\n\u00a9 2022 Kittenlord", "About Solitaire", JOptionPane.INFORMATION_MESSAGE);
	}

	void showHelpDialog() {
		String help;
		try {
			help = String.join("\n", Resources.getText(config.getString("file.help")));
		} catch (IOException e) {
			help = e.getMessage();
		}
		var area = new JTextArea(help);
		area.setEditable(false);
		area.setBorder(new EmptyBorder(10, 10, 10, 10));
		showDialog(area, "Instructions");
	}

	void showHiScoreDialog() {
		var model = new DefaultTableModel();
		model.addColumn("Level");
		model.addColumn("Score");
		for (var level : config.getKeys("level")) {
			var name = config.getString("level.%s.name", level);
			var dataFileName = config.getString("level.%s.data", level);
			try {
				var levelKey = GameState.fromStringList(Resources.getText(dataFileName)).toOneLineString();
				var hi = hiscore.getProperty((String) levelKey);
				model.addRow(new Object[] { name, (null == hi) ? "N/A" : ("" + hi) });
			} catch (IOException e) {
				model.addRow(new Object[] { name, "error" });
			}
		}
		var table = new JTable(model);
		table.getColumnModel().getColumn(0).setPreferredWidth(200);
		table.getColumnModel().getColumn(1).setPreferredWidth(50);
		showDialog(table, "Hi Score");
	}

	private void showDialog(JComponent component, String title) {
		var dialog = new JDialog(frame, title);
		var close = new JButton("Close");
		close.addActionListener(e -> dialog.dispose()); // must be before dialog.setVisible
		dialog.add(component);
		dialog.add(close, BorderLayout.PAGE_END);
		dialog.pack();
		dialog.setModalityType(ModalityType.APPLICATION_MODAL);
		dialog.setVisible(true);
	}

}
