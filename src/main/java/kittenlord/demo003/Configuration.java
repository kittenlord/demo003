package kittenlord.demo003;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * Provides textual and numberic values stored in a configuration file.
 */
public class Configuration {

	private Properties props;

	public Configuration(InputStream is) throws IOException {
		props = new Properties();
		props.load(is);
	}

	private static String beforeFirstDot(String s) {
		int p = s.indexOf('.');
		return (p < 0) ? s : s.substring(0, p);
	}

	/**
	 * Returns the String value for given key in configuration; throws an exception if there is no such value.
	 */
	public String getString(String keyFormat, Object... keyArguments) {
		String key = String.format(keyFormat, keyArguments);
		if (!props.containsKey(key)) {
			throw new IllegalArgumentException("Missing key in configuration: " + key);
		}
		return props.getProperty(key);
	}

	public int getInteger(String key) {
		var string = getString(key);
		return Integer.decode(string);
	}

	/**
	 * Returns a list of words that can follow after the provided word, sorted alphabetically. For example, if the
	 * configuration file contains a key "one.two" or "one.two.three", then the results of getKeys("one") will contain
	 * "two".
	 */
	public List<String> getKeys(String parentKey) {
		String prefix = parentKey + ".";
		return props.stringPropertyNames().stream() //
				.filter(s -> s.startsWith(prefix)) //
				.map(s -> s.substring(prefix.length())) //
				.map(Configuration::beforeFirstDot) //
				.distinct() //
				.sorted() //
				.collect(Collectors.toList());
	}

}
