package kittenlord.demo003;

/**
 * One of the four basic directions on screen.
 */
public enum Direction {
	UP(0, -1),
	RIGHT(1, 0),
	DOWN(0, 1),
	LEFT(-1, 0);

	private final int dx;
	private final int dy;

	private Direction(int dx, int dy) {
		this.dx = dx;
		this.dy = dy;
	}

	/**
	 * The horizontal component, in screen coordinates.
	 */
	public int getX() {
		return dx;
	}

	/**
	 * The vertical component, in screen coordinates.
	 */
	public int getY() {
		return dy;
	}


}
