package kittenlord.demo003;

import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

import javax.imageio.ImageIO;

public class Resources {

	private static final String CONFIG_FILE_NAME = "config.properties";

	private Resources() {
		throw new IllegalStateException("no instances");
	}

	public static Configuration getConfiguration() throws IOException {
		return new Configuration(Resources.class.getResourceAsStream(CONFIG_FILE_NAME));
	}

	public static BufferedImage getImage(String imageFileName) {
		try (var is = Resources.class.getResourceAsStream(imageFileName)) {
			if (null == is) {
				throw new IllegalStateException(String.format("Resource '%s' not found", imageFileName));
			}
			return ImageIO.read(is);
		} catch (IOException e) {
			throw new IllegalStateException(String.format("Error loading resource '%s'", imageFileName), e);
		}
	}

	public static List<String> getText(String fileName) throws IOException {
		try (
				var is = Resources.class.getResourceAsStream(fileName);
				var isr = new InputStreamReader(is);
				var reader = new BufferedReader(isr); //
		) {
			return reader.lines().collect(Collectors.toList());
		}
	}

	private static String getHiScorePath(Configuration config) {
		String hiscoreFileName = config.getString("file.hiscore");
		String homePath = System.getProperty("user.home");
		return (null != homePath) ? (homePath + File.separator + hiscoreFileName) : hiscoreFileName;
	}
	
	public static Properties loadHiScore(Configuration config) {
		Properties p = new Properties();
		try (var is = new FileInputStream(getHiScorePath(config))) {
			p.load(is);
		} catch (FileNotFoundException e) {
			// ignore
		} catch (IOException e) {
			// ignore
		}
		return p;
	}


	public static void saveHiScore(Properties hiscore, Configuration config) {
		try (var os = new FileOutputStream(getHiScorePath(config))) {
			hiscore.store(os, "Solitaire (demo003) HiScore File");
		} catch (FileNotFoundException e) {
			// ignore
		} catch (IOException e) {
			// ignore
		}
	}

}
