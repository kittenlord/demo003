package kittenlord.demo003;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

public class GameState {

	enum FieldType {
		EMPTY, PIECE;
	}

	private static final char SAVE_PIECE = 'x';
	private static final char SAVE_EMPTY = 'o';
	private static final char SAVE_OUTSIDE = '.';

	private int boardWidth;
	private int boardHeight;

	private Map<Position, FieldType> fields = new HashMap<>();
	private Position selected = null;

	private GameState(Map<Position, FieldType> pieces) {
		this.fields = pieces;
		boardWidth = 1 + pieces.keySet().stream().mapToInt(Position::getX).max().orElse(-1);
		boardHeight = 1 + pieces.keySet().stream().mapToInt(Position::getY).max().orElse(-1);
	}

	public static GameState fromStringList(List<String> list) {
		Map<Position, FieldType> pieces = new HashMap<>();
		for (int y = 0; y < list.size(); y++) {
			String line = list.get(y);
			for (int x = 0; x < line.length(); x++) {
				char c = line.charAt(x);
				if (SAVE_PIECE == c) {
					pieces.put(new Position(x, y), FieldType.PIECE);
				}
				if (SAVE_EMPTY == c) {
					pieces.put(new Position(x, y), FieldType.EMPTY);
				}
			}
		}
		return new GameState(startingAtZero(pieces));
	}

	/**
	 * Returns the map with positions moved so that both X and Y coordinates start at zero.
	 */
	private static <E> Map<Position, E> startingAtZero(Map<Position, E> map) {
		if (map.isEmpty()) {
			return map;
		}
		int minX = map.keySet().stream().mapToInt(Position::getX).min().getAsInt();
		int minY = map.keySet().stream().mapToInt(Position::getY).min().getAsInt();
		if (0 == minX && 0 == minY) {
			return map;
		}
		return map.entrySet().stream()
				.collect(Collectors.toMap(e -> e.getKey().moved(-minX, -minY), Map.Entry::getValue));
	}

	public int getWidth() {
		return boardWidth;
	}

	public int getHeight() {
		return boardHeight;
	}

	public FieldType getFieldAt(Position position) {
		return fields.get(position);
	}

	public boolean isSelected(Position position) {
		return Objects.equals(position, selected);
	}

	public boolean contains(Position position) {
		return fields.containsKey(position);
	}

	public boolean hasEmptyAt(Position position) {
		return fields.containsKey(position) && FieldType.EMPTY == fields.get(position);
	}

	public boolean hasPieceAt(Position position) {
		return fields.containsKey(position) && FieldType.PIECE == fields.get(position);
	}

	public boolean canMove(Position startPosition, Position endPosition) {
		var directionOpt = startPosition.directionTo(endPosition);
		if (directionOpt.isEmpty()) {
			return false;
		}
		var direction = directionOpt.get();
		return startPosition.moved(direction, 2).equals(endPosition) //
				&& hasPieceAt(startPosition) //
				&& hasPieceAt(startPosition.moved(direction, 1)) //
				&& hasEmptyAt(endPosition);
	}

	public boolean canMoveAnywhere() {
		for (var startPosition : fields.keySet()) {
			for (var endPosition : fields.keySet()) {
				if (canMove(startPosition, endPosition)) {
					return true;
				}
			}
		}
		return false;
	}

	/**
	 * Processes a mouse click at certain coordinate; returns true if a move was made as a result of the click.
	 */
	public boolean clickAt(Position clicked) {
		if (null != selected) {
			if (canMove(selected, clicked)) {
				var direction = selected.directionTo(clicked).get();
				fields.put(selected, FieldType.EMPTY);
				fields.put(selected.moved(direction, 1), FieldType.EMPTY);
				fields.put(selected.moved(direction, 2), FieldType.PIECE);
				selected = clicked;
				return true;
			}
		}
		if (hasPieceAt(clicked)) {
			selected = clicked;
			return false;
		}
		selected = null;
		return false;
	}

	public int piecesRemaining() {
		return fields.values().stream().mapToInt(ft -> (FieldType.PIECE == ft) ? 1 : 0).sum();
	}

	public List<String> toStringList() {
		List<String> result = new ArrayList<>();
		for (int y = 0; y < boardHeight; y++) {
			StringBuilder line = new StringBuilder();
			for (int x = 0; x < boardWidth; x++) {
				var position = new Position(x, y);
				if (fields.containsKey(position)) {
					line.append(FieldType.PIECE == fields.get(position) ? SAVE_PIECE : SAVE_EMPTY);
				} else {
					line.append(SAVE_OUTSIDE);
				}
			}
			result.add(line.toString());
		}
		return result;
	}

	@Override
	public String toString() {
		return toString("\n");
	}

	public String toOneLineString() {
		return toString(":");
	}

	private String toString(String separator) {
		return String.join(separator, toStringList());
	}

}
