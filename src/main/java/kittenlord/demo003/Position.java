package kittenlord.demo003;

import java.util.Objects;
import java.util.Optional;

/**
 * Two coordinates.
 */
public class Position {

	private final int x;
	private final int y;

	public Position(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	/**
	 * Move the position by given vector.
	 */
	public Position moved(int dx, int dy) {
		return new Position(x + dx, y + dy);
	}

	/**
	 * Move the position in given direction.
	 */
	public Position moved(Direction direction, int steps) {
		return moved(steps * direction.getX(), steps * direction.getY());
	}

	/**
	 * Returns the direction from this position to target position; or empty if it is not in a straight line.
	 */
	public Optional<Direction> directionTo(Position target) {
		if ((target.x == x) && (target.y < y)) {
			return Optional.of(Direction.UP);
		}
		if ((target.x == x) && (target.y > y)) {
			return Optional.of(Direction.DOWN);
		}
		if ((target.y == y) && (target.x < x)) {
			return Optional.of(Direction.LEFT);
		}
		if ((target.y == y) && (target.x > x)) {
			return Optional.of(Direction.RIGHT);
		}
		return Optional.empty();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (!(obj instanceof Position)) {
			return false;
		}
		Position that = (Position) obj;
		return this.x == that.x && this.y == that.y;
	}

	@Override
	public int hashCode() {
		return Objects.hash(x, y);
	}

	@Override
	public String toString() {
		return "(" + x + "," + y + ")";
	}

}
