Select a piece, then skip over one, thus removing it, like this:

	O O _
	 -->
	_ _ O

Repeat until no more moves are possible. Try to have as few remaining pieces as possible.

You select and move by clicking, not dragging.
Click on a piece to select, then click on an empty space to move.
